package br.com.framework.starwars258.database;

import java.util.List;

/**
 * Created by felipe.arimateia on 3/7/2016.
 */
public interface Dao<T> {

    List<T> getAll();
    boolean insert(T object);
    T getById(String id);
    boolean update(T object);
    boolean delete(T object);
    void close();
}
