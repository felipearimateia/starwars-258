package br.com.framework.starwars258;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import br.com.framework.starwars258.modelo.Droid;
import butterknife.Bind;
import butterknife.ButterKnife;

public class DroidActivity extends AppCompatActivity {

    private Droid droid;

    @Bind(R.id.droid_image)
    AppCompatImageView droidImage;

    @Bind(R.id.droid_description)
    AppCompatTextView droidDescription;

    @Bind(R.id.droid_font)
    AppCompatTextView droidFont;

    @Nullable
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_droid);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        droid = (Droid) getIntent().getSerializableExtra("droid");

        setTitle(droid.getName());

        Picasso.with(this).load(droid.getImage()).into(droidImage);
        droidDescription.setText(droid.getDescription());
        droidFont.setText("Fonte: " + droid.getLink());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_droid, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_share) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, droid.getName() + " - " + droid.getLink());
            intent.setType("text/plain");
            startActivity(intent);
            return true;
        }
        else if (item.getItemId() == R.id.action_save_image) {
            saveImage();
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveImage() {
        Picasso.with(this).load(droid.getImage()).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                if (bitmap == null) {
                    Toast.makeText(DroidActivity.this,
                            "Não é possivel salvar imagem", Toast.LENGTH_SHORT).show();
                    return;
                }

                File imageFile = new File(Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                        ,"/StarWars258/" + "droid_"+System.currentTimeMillis() + ".jpg");

                FileOutputStream outputStream = null;

                try {
                    outputStream = new FileOutputStream(imageFile);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                } catch (FileNotFoundException e) {
                    Log.e("SAVE_IMAGE", "Error ao salvar imagem.", e);
                }finally {
                    if (outputStream != null) {
                        try {
                            outputStream.close();
                        } catch (IOException e) {
                            Log.e("SAVE_IMAGE", "Error ao salvar imagem.", e);
                        }
                    }
                }

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }
}
