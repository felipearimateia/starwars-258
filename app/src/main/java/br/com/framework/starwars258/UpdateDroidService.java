package br.com.framework.starwars258;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import br.com.framework.starwars258.database.DroidDao;
import br.com.framework.starwars258.modelo.Droid;

/**
 * Created by felipe.arimateia on 3/14/2016.
 */
public class UpdateDroidService extends Service {

    public static final String ACTION_DROID_UPDATE = "action_adroid_update";


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null || TextUtils.isEmpty(intent.getAction())) {
            return super.onStartCommand(intent, flags, startId);
        }

        if (ACTION_DROID_UPDATE.equals(intent.getAction())) {
            Droid droid = (Droid) intent.getSerializableExtra("droid");
            saveDroid(droid);
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void saveDroid(Droid droid) {
        DroidDao droidDao = new DroidDao(this);
        droidDao.insert(droid);
    }
}
