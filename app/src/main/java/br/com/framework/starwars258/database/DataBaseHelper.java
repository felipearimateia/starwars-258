package br.com.framework.starwars258.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by felipe.arimateia on 3/7/2016.
 */
public class DataBaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "starwars.db";

    private final String DDL_DROIDS = "CREATE TABLE [droids] (\n" +
            "  [id] TEXT, \n" +
            "  [name] VARCHAR(100), \n" +
            "  [description] TEXT, \n" +
            "  [link] TEXT, \n" +
            "  [image] TEXT, \n" +
            "  CONSTRAINT [] PRIMARY KEY ([id]));\n";

    private final String DDL_FILMS = "CREATE TABLE [films] (\n" +
            "  [id] TEXT NOT NULL, \n" +
            "  [name] VARCHAR(100), \n" +
            "  [description] TEXT, \n" +
            "  [year] INT, \n" +
            "  [image] TEXT, \n" +
            "  [link] TEXT, \n" +
            "  CONSTRAINT [] PRIMARY KEY ([id]));";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DDL_DROIDS);
        db.execSQL(DDL_FILMS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static SQLiteDatabase getDatabase(Context context) {
        DataBaseHelper databaseHelper = new DataBaseHelper(context);
        return databaseHelper.getWritableDatabase();
    }
}
