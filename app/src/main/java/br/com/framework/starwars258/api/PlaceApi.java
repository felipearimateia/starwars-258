package br.com.framework.starwars258.api;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;

import br.com.framework.starwars258.modelo.Place;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by felipe.arimateia on 3/9/2016.
 */
public class PlaceApi extends BaseApi {

    public void locations(final OperationResultListener<List<Place>> listener){

        Request request = new Request.Builder()
                .url(API_LOCATIONS)
                .get().build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                listener.onResultOperation(null, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {
                    Gson gson = new Gson();
                    List<Place> places = gson.fromJson(response.body().charStream(),
                            new TypeToken<List<Place>>(){}.getType());

                    listener.onResultOperation(places, null);
                }
                else {
                    listener.onResultOperation(null,
                            new Exception("Error desconhecido " + response.code()));
                }
            }
        });
    }
}
