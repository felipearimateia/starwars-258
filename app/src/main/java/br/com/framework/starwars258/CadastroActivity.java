package br.com.framework.starwars258;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import br.com.framework.starwars258.api.BaseApi;
import br.com.framework.starwars258.api.UserApi;
import br.com.framework.starwars258.modelo.User;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CadastroActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_GALLERY = 1001;
    public static final int REQUEST_CODE_CADASTRO = 1002;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.img_perfil)
    AppCompatImageView imgPerfil;
    @Bind(R.id.edit_nome)
    AppCompatEditText editNome;
    @Bind(R.id.edit_email)
    AppCompatEditText editEmail;
    @Bind(R.id.edit_senha)
    AppCompatEditText editSenha;
    @Bind(R.id.edit_confrimar_senha)
    AppCompatEditText editConfrimarSenha;
    @Bind(R.id.btn_ok)
    AppCompatButton btnOk;

    private String photoPath;
    private UserApi userApi;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        ButterKnife.bind(this);
        userApi = new UserApi();
    }

    @OnClick(R.id.img_perfil)
    public void onClick() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE_GALLERY);
    }
    @OnClick(R.id.btn_ok)
    public  void onClickOk() {

        progressDialog = ProgressDialog.show(this, getString(R.string.app_name),
                "Aguarde...", true, false);

        User user = new User();
        user.setName(editNome.getText().toString());
        user.setEmail(editEmail.getText().toString());
        user.setPassword(editSenha.getText().toString());

        userApi.registrar(user, new BaseApi.OperationResultListener<User>() {
            @Override
            public void onResultOperation(final User result, Throwable error) {

               runOnUiThread(new Runnable() {
                   @Override
                   public void run() {

                       if (result != null) {
                           uploadPhoto(result);
                       }
                       else {
                           if (progressDialog != null && progressDialog.isShowing()) {
                               progressDialog.dismiss();
                           }

                           Toast.makeText(CadastroActivity.this, "Ops! Estou com zica!",
                                   Toast.LENGTH_SHORT).show();
                       }
                   }
               });
            }
        });
    }

    private void uploadPhoto(User result) {
        if (TextUtils.isEmpty(photoPath)) {
            finishCadastro(result);
            return;
        }

        userApi.uploadPhoto(result.getId(), photoPath, new BaseApi.OperationResultListener<User>() {
            @Override
            public void onResultOperation(final User result, Throwable error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                        if (result != null) {
                            finishCadastro(result);
                        }
                        else {

                            Toast.makeText(CadastroActivity.this, "Ops! Estou com zica!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    private void finishCadastro(User user) {
        Intent intent = new Intent();
        intent.putExtra("user", user);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_GALLERY) {
            Uri uri = data.getData();
            Picasso.with(this).load(uri).into(imgPerfil);

            photoPath = convertMediaUriToPath(uri);
        }
    }

    private String convertMediaUriToPath(Uri uri) {
        String [] proj={MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(column_index);
        cursor.close();
        return path;
    }
}
