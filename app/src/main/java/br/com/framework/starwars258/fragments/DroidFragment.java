package br.com.framework.starwars258.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.framework.starwars258.MockStarWars;
import br.com.framework.starwars258.R;
import br.com.framework.starwars258.Utils;
import br.com.framework.starwars258.adapter.DroidsAdapter;
import br.com.framework.starwars258.adapter.FilmeAdapter;
import br.com.framework.starwars258.api.BaseApi;
import br.com.framework.starwars258.api.DroidApi;
import br.com.framework.starwars258.database.Dao;
import br.com.framework.starwars258.database.DroidDao;
import br.com.framework.starwars258.modelo.Droid;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by felipe.arimateia on 2/23/2016.
 */
public class DroidFragment extends Fragment {

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    @Bind(R.id.progress_bar)
    ContentLoadingProgressBar progressBar;

    private DroidApi droidApi;
    private DroidsAdapter adapter;

    public static DroidFragment newInstance() {
        DroidFragment fragment = new DroidFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        droidApi = new DroidApi();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filmes, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new DroidsAdapter(getActivity(), new ArrayList<Droid>(), recyclerView);
        recyclerView.setAdapter(adapter);

        if (Utils.isConnected(getActivity())) {
            getDroidApi();
        } else {
            getDroidDatabase();
        }
    }

    private void getDroidDatabase() {

        new AsyncTask<Void, Void, List<Droid>>(){

            @Override
            protected List<Droid> doInBackground(Void... params) {
                Dao<Droid> dao = new DroidDao(getActivity());
                List<Droid> droids = dao.getAll();
                dao.close();
                return droids;
            }

            @Override
            protected void onPostExecute(List<Droid> droids) {
                super.onPostExecute(droids);

                adapter.addAll(droids);
                progressBar.hide();
            }

        }.execute();
    }

    private void getDroidApi() {

        droidApi.getDroids(getActivity(), new BaseApi.OperationResultListener<List<Droid>>() {
            @Override
            public void onResultOperation(final List<Droid> result, Throwable error) {

              getActivity().runOnUiThread(new Runnable() {
                  @Override
                  public void run() {
                      if (result != null) {
                          adapter.addAll(result);
                      }
                      else {
                          Toast.makeText(getActivity(), "Ops! Desculpa estou com zica.",
                                  Toast.LENGTH_SHORT).show();
                      }
                      progressBar.hide();
                  }
              });
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
