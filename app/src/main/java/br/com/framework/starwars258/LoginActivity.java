package br.com.framework.starwars258;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.Toast;

import br.com.framework.starwars258.modelo.User;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.imageView2)
    ImageView imageView2;
    @Bind(R.id.editTextUser)
    AppCompatEditText editTextUser;
    @Bind(R.id.editTextSenha)
    AppCompatEditText editTextSenha;
    @Bind(R.id.buttonRegistrar)
    AppCompatButton buttonRegistrar;
    @Bind(R.id.buttonEntrar)
    AppCompatButton buttonEntrar;
    @Bind(R.id.save_login)
    AppCompatCheckBox checkSaveLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
    }

    @OnClick(R.id.buttonEntrar)
    public void onClickEntrar() {
        String usuario = editTextUser.getText().toString();
        String senha = editTextSenha.getText().toString();

        if (checkSaveLogin.isChecked()) {
            SharedPreferences preferences = getSharedPreferences("CONFIGURACOES", MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("save_login", true);
            editor.commit();
        }

        if (TextUtils.isEmpty(usuario) || TextUtils.isEmpty(senha)) {
            editTextSenha.setError(getString(R.string.msg_erro_senha));
        } else {
            Toast.makeText(LoginActivity.this, R.string.msg_logado,
                    Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @OnClick(R.id.buttonRegistrar)
    public void onClickRegistrar() {
        Intent intent = new Intent(LoginActivity.this, CadastroActivity.class);
        startActivityForResult(intent, CadastroActivity.REQUEST_CODE_CADASTRO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == CadastroActivity.REQUEST_CODE_CADASTRO) {
            User user = (User) data.getSerializableExtra("user");
            finish();
        }
    }
}
