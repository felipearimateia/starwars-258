package br.com.framework.starwars258.api;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;

/**
 * Created by felipe.arimateia on 3/2/2016.
 */
public class BaseApi {

    private static final String BASE_URL = "http://arimateia.info:3005";
    protected static String API_LOGIN = BASE_URL + "/users/login";
    protected static String API_SIGNUP = BASE_URL + "/users/signup";
    protected static String API_DROIDS = BASE_URL + "/droids";
    protected static String API_FILMS = BASE_URL + "/films";
    protected static String API_LOCATIONS = BASE_URL + "/locations";
    public static String API_SAVE_DEVICE_TOKEN = BASE_URL + "/devices/save/%s";
    protected static String BASE_URL_IMAGE = BASE_URL + "/images/%s";
    protected static String API_UPLOAD_PHOTO_USER = BASE_URL + "/users/uploadPhoto/%s";

    protected static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json");
    protected static final MediaType MEDIA_TYPE_IMAGE = MediaType.parse("image/jpg");

    public static final OkHttpClient okHttpClient = new OkHttpClient();

    public BaseApi() {
    }

    public interface OperationResultListener<T> {
        void onResultOperation(T result, Throwable error);
    }
}
