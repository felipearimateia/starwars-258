package br.com.framework.starwars258.api;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;

import br.com.framework.starwars258.database.DroidDao;
import br.com.framework.starwars258.modelo.Droid;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by felipe.arimateia on 3/2/2016.
 */
public class DroidApi extends BaseApi {

    public void getDroids(final Context context, final OperationResultListener<List<Droid>> listener) {

        Request request = new Request.Builder()
                .url("http://arimateia.info:3005/droids")
                .get()
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                listener.onResultOperation(null, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {
                    Gson gson = new Gson();
                    List<Droid> droids = gson.fromJson(response.body().charStream(),
                            new TypeToken<List<Droid>>(){}.getType());
                    listener.onResultOperation(droids, null);
                    saveDroids(context, droids);
                }
                else {
                    listener.onResultOperation(null,
                            new Exception("Error desconhecido " + response.code()));
                }
            }
        });
    }

    private void saveDroids(Context context, List<Droid> droids) {
        DroidDao droidDao = new DroidDao(context);
        for (Droid droid : droids) {
            droidDao.insert(droid);
        }
        droidDao.close();
    }

}
