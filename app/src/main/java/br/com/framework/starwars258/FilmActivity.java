package br.com.framework.starwars258;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import br.com.framework.starwars258.modelo.Characters;
import br.com.framework.starwars258.modelo.Film;
import butterknife.Bind;
import butterknife.ButterKnife;

public class FilmActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.image_filme)
    AppCompatImageView imageFilme;
    @Bind(R.id.nome_filme)
    AppCompatTextView nomeFilme;
    @Bind(R.id.ano_filme)
    AppCompatTextView anoFilme;
    @Bind(R.id.desc_filme)
    AppCompatTextView descFilme;

    @Bind(R.id.content_characters)
    LinearLayout contentCharacters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Film film = (Film) getIntent().getSerializableExtra("film");

        Picasso.with(this).load(film.getImage()).into(imageFilme);
        nomeFilme.setText(film.getName());
        anoFilme.setText(String.valueOf(film.getYear()));
        descFilme.setText(film.getDescription());

        for (Characters characters : film.getCharacters()) {
            View view = LayoutInflater.from(this).inflate(R.layout.item_characters, contentCharacters,false);
            AppCompatImageView charactersImage = ButterKnife.findById(view, R.id.characters_image);
            AppCompatTextView charactersName = ButterKnife.findById(view, R.id.characters_name);

            Picasso.with(this).load(characters.getImage()).into(charactersImage);
            charactersName.setText(characters.getName());

            contentCharacters.addView(view);
        }
    }

}
