package br.com.framework.starwars258.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;

import br.com.framework.starwars258.PlaceActivity;
import br.com.framework.starwars258.R;
import br.com.framework.starwars258.api.BaseApi;
import br.com.framework.starwars258.api.PlaceApi;
import br.com.framework.starwars258.modelo.Place;

/**
 * Created by felipe.arimateia on 3/8/2016.
 */
public class PlacesFragment extends Fragment {

    private GoogleMap googleMap;
    private HashMap<Marker, Place> markerPlaces = new HashMap<>();

    public static PlacesFragment newInstance() {
        PlacesFragment fragment = new PlacesFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_places, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SupportMapFragment fragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.fragment_google_maps);
        fragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                PlacesFragment.this.googleMap = googleMap;
                configGoogleMap();
                getPlaces();
            }
        });
    }

    private void configGoogleMap() {
        googleMap.setMyLocationEnabled(true);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setOnInfoWindowClickListener(onInfoWindowClickListener);

        UiSettings settings = googleMap.getUiSettings();
        settings.setCompassEnabled(true);
        settings.setZoomControlsEnabled(true);
        settings.setAllGesturesEnabled(true);
        settings.setMyLocationButtonEnabled(true);
    }

    private void getPlaces() {
        PlaceApi api = new PlaceApi();
        api.locations(new BaseApi.OperationResultListener<List<Place>>() {
            @Override
            public void onResultOperation(final List<Place> result, Throwable error) {

                if (result != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            addMarkes(result);
                        }
                    });
                }
                else {
                    Toast.makeText(getActivity(),"Estou com zica!",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addMarkes(List<Place> places) {

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for (Place place: places) {

            LatLng position = new LatLng(place.getLatitude(), place.getLongitude());
            builder.include(position);

            MarkerOptions markerOptions = new MarkerOptions()
                    .title(place.getTitle())
                    .snippet(place.getDescription())
                    .position(position);

            Marker marker = googleMap.addMarker(markerOptions);
            markerPlaces.put(marker,place);
        }

        LatLngBounds bounds = builder.build();

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 50);
        googleMap.moveCamera(cameraUpdate);
    }

    private GoogleMap.OnInfoWindowClickListener onInfoWindowClickListener =
            new GoogleMap.OnInfoWindowClickListener() {
        @Override
        public void onInfoWindowClick(Marker marker) {
            Place place = markerPlaces.get(marker);

            Intent intent = new Intent(getActivity(), PlaceActivity.class);
            intent.putExtra("place",place);
            startActivity(intent);

        }
    };
}
