package br.com.framework.starwars258.api;

import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;

import br.com.framework.starwars258.modelo.User;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by felipe.arimateia on 3/3/2016.
 */
public class UserApi extends BaseApi{


    public void registrar(User user, final OperationResultListener<User> listener) {
        final Gson gson = new Gson();
        String json = gson.toJson(user);

        Request request = new Request.Builder()
                .url(API_SIGNUP)
                .post(RequestBody.create(MEDIA_TYPE_JSON, json))
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                listener.onResultOperation(null,e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    User user = gson.fromJson(response.body().charStream(), User.class);
                    listener.onResultOperation(user, null);
                }
                else {
                    listener.onResultOperation(null, new Exception("Erro cadastrar usuario"));
                }
            }
        });
    }


    public void uploadPhoto(String userId, String path,
                            final OperationResultListener<User> listener) {

        File file = new File(path);

        MultipartBody multipartBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", file.getName(),
                        RequestBody.create(MEDIA_TYPE_IMAGE, file))
                .build();

        Request request = new Request.Builder()
                .url(String.format(API_UPLOAD_PHOTO_USER, userId))
                .post(multipartBody)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                listener.onResultOperation(null,e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    Gson gson = new Gson();
                    User user = gson.fromJson(response.body().charStream(), User.class);
                    listener.onResultOperation(user, null);
                }
                else {
                    listener.onResultOperation(null, new Exception("Erro cadastrar usuario"));
                }
            }
        });
    }

    public void login(User user, final OperationResultListener<User> listener) {

        final Gson gson = new Gson();
        String json = gson.toJson(user);

        Request request = new Request.Builder()
                .url(API_LOGIN)
                .post(RequestBody.create(MEDIA_TYPE_JSON, json))
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                listener.onResultOperation(null,e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    Gson gson = new Gson();
                    User user = gson.fromJson(response.body().charStream(), User.class);
                    listener.onResultOperation(user, null);
                }
                else {
                    listener.onResultOperation(null, new Exception("Erro ao logar usuario"));
                }
            }
        });
    }

}
