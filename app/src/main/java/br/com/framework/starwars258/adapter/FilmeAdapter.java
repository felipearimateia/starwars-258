package br.com.framework.starwars258.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.framework.starwars258.DroidActivity;
import br.com.framework.starwars258.FilmActivity;
import br.com.framework.starwars258.R;
import br.com.framework.starwars258.modelo.Droid;
import br.com.framework.starwars258.modelo.Film;

/**
 * Created by felipe.arimateia on 2/23/2016.
 */
public class FilmeAdapter extends RecyclerView.Adapter<FilmeAdapter.ViewHolder>{

    private Context context;
    private List<Film> films;
    private RecyclerView recyclerView;

    public FilmeAdapter(Context context, List<Film> films, RecyclerView recyclerView) {
        this.context = context;
        this.films = films;
        this.recyclerView = recyclerView;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_filme, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Film film = films.get(position);
        Picasso.with(context).load(film.getImage()).into(holder.imageFilme);
        holder.nomeFile.setText(film.getName());
        holder.anoFilme.setText(String.valueOf(film.getYear()));
    }

    @Override
    public int getItemCount() {
        return films.size();
    }

    public void addAll(List<Film> result) {
        if (this.films == null) {
            this.films = new ArrayList<>(result);
        }
        else {
            this.films.addAll(result);
        }
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        AppCompatImageView imageFilme;
        AppCompatTextView nomeFile;
        AppCompatTextView anoFilme;

        public ViewHolder(final View itemView) {
            super(itemView);

            imageFilme = (AppCompatImageView)itemView.findViewById(R.id.image_filme);
            nomeFile = (AppCompatTextView)itemView.findViewById(R.id.nome_filme);
            anoFilme = (AppCompatTextView)itemView.findViewById(R.id.ano_filme);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = recyclerView.getChildAdapterPosition(itemView);
                    Film film = films.get(position);

                    Intent intent = new Intent(context, FilmActivity.class);
                    intent.putExtra("film", film);
                    context.startActivity(intent);
                }
            });
        }
    }
}
