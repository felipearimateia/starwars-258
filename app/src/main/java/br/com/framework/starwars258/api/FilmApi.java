package br.com.framework.starwars258.api;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;

import br.com.framework.starwars258.modelo.Droid;
import br.com.framework.starwars258.modelo.Film;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by felipe.arimateia on 3/2/2016.
 */
public class FilmApi extends BaseApi {

    public void getFilm(final OperationResultListener<List<Film>> listener) {

        Request request = new Request.Builder()
                .url("http://arimateia.info:3005/films")
                .get()
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                listener.onResultOperation(null, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {
                    Gson gson = new Gson();
                    List<Film> films = gson.fromJson(response.body().charStream(),
                            new TypeToken<List<Film>>(){}.getType());

                    listener.onResultOperation(films, null);
                }
                else {
                    listener.onResultOperation(null,
                            new Exception("Error desconhecido " + response.code()));
                }
            }
        });
    }

}
