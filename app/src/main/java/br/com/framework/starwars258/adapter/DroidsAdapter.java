package br.com.framework.starwars258.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.framework.starwars258.DroidActivity;
import br.com.framework.starwars258.R;
import br.com.framework.starwars258.modelo.Droid;
import br.com.framework.starwars258.modelo.Film;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by felipe.arimateia on 2/23/2016.
 */
public class DroidsAdapter extends RecyclerView.Adapter<DroidsAdapter.ViewHolder>{

    private Context context;
    private List<Droid> droids;
    private RecyclerView recyclerView;

    public DroidsAdapter(Context context, List<Droid> droids, RecyclerView recyclerView) {
        this.context = context;
        this.droids = droids;
        this.recyclerView = recyclerView;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_droid, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Droid droid = droids.get(position);
        Picasso.with(context).load(droid.getImage()).into(holder.droidImage);
        holder.droidNome.setText(droid.getName());
        holder.droidDesc.setText(droid.getDescription());
    }

    @Override
    public int getItemCount() {
        return droids.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.droid_image)
        AppCompatImageView droidImage;
        @Bind(R.id.droid_name)
        AppCompatTextView droidNome;
        @Bind(R.id.droid_description)
        AppCompatTextView droidDesc;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = recyclerView.getChildAdapterPosition(itemView);
                    Droid droid = droids.get(position);

                    Intent intent = new Intent(context, DroidActivity.class);
                    intent.putExtra("droid", droid);
                    context.startActivity(intent);
                }
            });
        }
    }

    public void addAll(List<Droid> droids) {
        if (this.droids == null) {
            this.droids = new ArrayList<>(droids);
        }
        else {
            this.droids.addAll(droids);
        }
        notifyDataSetChanged();
    }
}
