package br.com.framework.starwars258.modelo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by felipe.arimateia on 12/3/2015.
 */
public class Characters implements Serializable{

    @SerializedName("_id")
    private String id;
    private String name;
    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
