package br.com.framework.starwars258.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.framework.starwars258.MockStarWars;
import br.com.framework.starwars258.R;
import br.com.framework.starwars258.adapter.FilmeAdapter;
import br.com.framework.starwars258.api.BaseApi;
import br.com.framework.starwars258.api.FilmApi;
import br.com.framework.starwars258.modelo.Film;

/**
 * Created by felipe.arimateia on 2/23/2016.
 */
public class FilmesFragment extends Fragment {

    private FilmApi filmApi;
    private ContentLoadingProgressBar progressBar;
    private FilmeAdapter adapter;

    public static FilmesFragment newInstance() {
        FilmesFragment fragment = new FilmesFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        filmApi = new FilmApi();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filmes, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        int colunas = getResources().getInteger(R.integer.colunas);

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), colunas);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new FilmeAdapter(getActivity(), new ArrayList<Film>(), recyclerView);
        recyclerView.setAdapter(adapter);

        progressBar =
                (ContentLoadingProgressBar) view.findViewById(R.id.progress_bar);

        filmApi.getFilm(new BaseApi.OperationResultListener<List<Film>>() {
            @Override
            public void onResultOperation(final List<Film> result, Throwable error) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (result != null) {
                            adapter.addAll(result);
                        }
                        else {
                            Toast.makeText(getActivity(), "Ops! Desculpa estou com zica.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        progressBar.hide();
                    }
                });
            }
        });
    }
}
