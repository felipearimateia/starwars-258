package br.com.framework.starwars258;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.squareup.picasso.Picasso;

import br.com.framework.starwars258.modelo.Place;
import butterknife.Bind;
import butterknife.ButterKnife;

public class PlaceActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.place_image)
    AppCompatImageView placeImage;
    @Bind(R.id.place_description)
    AppCompatTextView placeDescription;
    @Bind(R.id.user_location)
    AppCompatTextView userLocation;
    private Place place;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation;
    private LocationReciver locationReciver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        place = (Place) getIntent().getSerializableExtra("place");

        setTitle(place.getTitle());
        Picasso.with(this).load(place.getImage()).into(placeImage);
        placeDescription.setText(place.getDescription());

        createLocationRequest();
    }

    @Override
    protected void onResume() {
        super.onResume();
        buildGoogleApiClient();

        locationReciver = new LocationReciver();
        registerReceiver(locationReciver,
                new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
            mGoogleApiClient.disconnect();
        }

        if (locationReciver != null){
            unregisterReceiver(locationReciver);
        }
    }

    protected void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {

        mCurrentLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);

        if (mCurrentLocation != null) {
            showUserLocation(mCurrentLocation);
        }

        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        showUserLocation(mCurrentLocation);
    }

    private void showUserLocation(Location location) {

        float[] result = new float[1];

        Location.distanceBetween(location.getLatitude(), location.getLongitude(),
                place.getLatitude(), place.getLongitude(), result);

        float distance = (result[0]/1000);

        userLocation.setText(String.format("Você encotra-se a %.0f KM.", distance));
    }

    public class LocationReciver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            LocationManager locationManager = (LocationManager) context
                    .getSystemService(Context.LOCATION_SERVICE);

            String msg = "GPS Ligado!";

            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                msg = "GPS desligado";
            }

            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
        }
    }
}
