package br.com.framework.starwars258;

import android.app.Application;
import android.os.Environment;
import android.util.Log;

import com.facebook.stetho.Stetho;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by felipe.arimateia on 2/29/2016.
 */
public class StarWarsApp extends Application {

    //GCM
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this);
        }

        File cacheFile = getCacheDir();
        Log.i("CACHE_FILE", cacheFile.getAbsolutePath());

        File fileDirs = getFilesDir();
        Log.i("FILE_DIRS", fileDirs.getAbsolutePath());

        try {
            FileOutputStream outputStream = openFileOutput("user_configs.txt",MODE_PRIVATE);
            outputStream.write("token_do_usuario".getBytes());
            outputStream.close();
        } catch (java.io.IOException e) {
            Log.e("WRITE_FILE", "Falha ao escrever no arquivo user_configs.txt", e);
        }


        try {
            File userConfigFile = new File(getFilesDir(),"user_configs.txt");
            if (userConfigFile.exists()) {
                FileInputStream inputStream = openFileInput("user_configs.txt");
                byte[] buffer = new byte[1024];
                inputStream.read(buffer);
                String config = new String(buffer);
                Log.i("CONFIG",config);
            }
        } catch (java.io.IOException e) {
            Log.e("OPEN_FILE", "Falha ao abrir arquivo user_configs.txt", e);
        }

        //sandbox external
        File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "/StarWars258");
        if (!file.exists()) {
            file.mkdirs();
        }

        //diretorio externo publico
        if ( Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
            File externalFile = new File(Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                    ,"/StarWars258");

            if (!externalFile.exists()) {
                externalFile.mkdirs();
            }
        }
    }
}
