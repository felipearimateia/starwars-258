package br.com.framework.starwars258.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.framework.starwars258.modelo.Droid;

/**
 * Created by felipe.arimateia on 3/7/2016.
 */
public class DroidDao implements Dao<Droid> {

    private final SQLiteDatabase database;

    public DroidDao(Context context) {
        database = DataBaseHelper.getDatabase(context);
    }

    @Override
    public List<Droid> getAll() {
        List<Droid> droids = new ArrayList<>();

        Cursor cursor = database.query("droids", null, null, null, null, null, "name");
        cursor.moveToFirst();

        while (cursor.moveToNext()) {
            Droid droid = createDroid(cursor);
            droids.add(droid);
        }

        return droids;
    }

    @Override
    public boolean insert(Droid droid) {
        ContentValues contentValues = createContentValues(droid);
        long result = database.insertWithOnConflict("droids",null,contentValues,
                SQLiteDatabase.CONFLICT_REPLACE);

        if (result == -1) {
            return false;
        }

        return true;
    }

    @Override
    public Droid getById(String id) {
        return null;
    }

    @Override
    public boolean update(Droid object) {
        return false;
    }

    @Override
    public boolean delete(Droid object) {
        return false;
    }

    @Override
    public void close() {

    }

    private ContentValues createContentValues(Droid droid) {

        ContentValues contentValues = new ContentValues();
        contentValues.put("id", droid.getId());
        contentValues.put("name", droid.getName());
        contentValues.put("description", droid.getDescription());
        contentValues.put("link", droid.getLink());
        contentValues.put("image", droid.getImage());

        return contentValues;
    }

    private Droid createDroid(Cursor cursor) {

        Droid droid = new Droid();
        droid.setId(cursor.getString(cursor.getColumnIndex("id")));
        droid.setName(cursor.getString(cursor.getColumnIndex("name")));
        droid.setDescription(cursor.getString(cursor.getColumnIndex("description")));
        droid.setImage(cursor.getString(cursor.getColumnIndex("image")));
        droid.setLink(cursor.getString(cursor.getColumnIndex("link")));

        return droid;
    }
}
